import React from 'react';
import { ExpoConfigView } from '@expo/samples';
import {Constants} from "expo";
import {Image, SectionList, StyleSheet, Text, View} from "react-native";

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: '设置',
  };


  render() {
      return (
          <SectionList
              style={styles.container}
              renderItem={this._renderItem}
              renderSectionHeader={this._renderSectionHeader}
              stickySectionHeadersEnabled={true}
              keyExtractor={(item, index) => index}
              ListHeaderComponent={ListHeader}
              sections={sections}
          />
      )
  }

  _renderItem = ({ item }) => {
      return (
          <SectionContent>
              <Text style={styles.sectionContentText}>
                  {item.value}
              </Text>
          </SectionContent>
      );
  };
  _renderSectionHeader = ({ section }) => {
      return <SectionHeader title={section.title} />;
  };
}

const SectionHeader = ({ title }) => {
    return (
        <View style={styles.sectionHeaderContainer}>
            <Text style={styles.sectionHeaderText}>
                {title}
            </Text>
        </View>
    );
};

const SectionContent = props => {
    return (
        <View style={styles.sectionContentContainer}>
            {props.children}
        </View>
    );
};

const sections = [];

const ListHeader = () => {
    const { manifest } = Constants;
    return (
        <View style={styles.titleContainer}>
            <View style={styles.titleIconContainer}>
                <AppIconPreview iconUrl={manifest.iconUrl} />
            </View>

            <View style={styles.titleTextContainer}>
                <Text style={styles.nameText} numberOfLines={1}>
                    {manifest.name}
                </Text>

                <Text style={styles.slugText} numberOfLines={1}>
                    {manifest.slug}
                </Text>

                <Text style={styles.descriptionText}>
                    {manifest.description}
                </Text>
            </View>
        </View>
    );
};

const AppIconPreview = ({ iconUrl }) => {
    return (
        <Image
            source={{ uri: iconUrl }}
            style={{ width: 64, height: 64 }}
            resizeMode="cover"
        />
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    titleContainer: {
        paddingHorizontal: 15,
        paddingTop: 15,
        paddingBottom: 15,
        flexDirection: 'row',
    },
    titleIconContainer: {
        marginRight: 15,
        paddingTop: 2,
    },
    sectionHeaderContainer: {
        backgroundColor: '#fbfbfb',
        paddingVertical: 8,
        paddingHorizontal: 15,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: '#ededed',
    },
    sectionHeaderText: {
        fontSize: 14,
    },
    sectionContentContainer: {
        paddingTop: 8,
        paddingBottom: 12,
        paddingHorizontal: 15,
    },
    nameText: {
        fontWeight: '600',
        fontSize: 18,
    },
    slugText: {
        color: '#a39f9f',
        fontSize: 14,
        backgroundColor: 'transparent',
    },
    descriptionText: {
        fontSize: 14,
        marginTop: 6,
        color: '#4d4d4d',
    },
});