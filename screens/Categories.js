import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';

export default class Categories extends React.Component {
  static navigationOptions = {
    title: '分类',
  };

  render() {
    return (
      <ScrollView style={styles.container}>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    titleContainer: {
        paddingHorizontal: 15,
        paddingTop: 15,
        paddingBottom: 15,
        flexDirection: 'row',
    },
});
